﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using CsQuery;

namespace XboxMusicPlaylistCrawler
{
    struct TrackLevenshteinRating
    {
        public int Title;
        public int Artist;
        public int Album;
    }

    class DriverManager
    {
        
        private ChromeDriver _driver;
        private WebDriverWait _driverWait;

        public void SearchForTrack(TrackData track)
        {
            var searchElement = _driver.FindElement(By.Id("searchValue"));
            var searchButtonElement = _driver.FindElement(By.CssSelector("span.searchButton"));
            searchElement.Clear();
            searchElement.SendKeys(track.Title + " " + track.Artist);
            searchButtonElement.Click();
        }

        public void Start()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(60));
            _driverWait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
        }

        public void Quit()
        {
            if(_driver != null)
                _driver.Quit();
        }

        public void NavigateToUrl(string url)
        {
            _driver.Navigate().GoToUrl(url);
        }

        public IWebElement WaitUntil(Func<IWebDriver, IWebElement> condition)
        {
            return _driverWait.Until(condition);
        }

        public List<TrackData> GetPlaylistTracks(List<string> trackLinks)
        {
            var trackList = new List<TrackData>();
            var tracksProcessed = 0;

            using (var client = new WebClient())
            {
                foreach (var link in trackLinks)
                {
                    CQ dom = client.DownloadString(link);

                    var pos = link.LastIndexOf("/", StringComparison.Ordinal) + 1;
                    var trackLinkTrimmed = link.Substring(pos, link.Length - pos);

                    var track = new TrackData();
                    track.Title = ConvertAsciiValuesToCharacters(dom.Select("tr[data-uri$=\"" + trackLinkTrimmed + "\"] > td[class^=\"tl-cell tl-name\"] > div.tl-highlight > strong").Text());

                    if (!track.Title.Any())
                    {
                        tracksProcessed++;
                        Program._logWriter.WriteLine(tracksProcessed + " out of " + trackLinks.Count + " tracks processed");
                        Program._logWriter.WriteLine("Failed to load track: " + link + "\n(some spotify links are broken or point towards the wrong tracks)\n");
                        continue;
                    }

                    track.Artist = ConvertAsciiValuesToCharacters(dom.Select("tr[data-uri$=\"" + trackLinkTrimmed + "\"] > td[class^=\"tl-cell tl-name\"] > div.tl-highlight > div.tl-name-more").Text().Replace(",", "").Trim());
                    track.Artist = Regex.Split(track.Artist, "\n")[0];
                    track.Album = ConvertAsciiValuesToCharacters(dom.Select("header.header.header-album > section > div.h-data > h1.h-title").Text());

                    trackList.Add(track);

                    tracksProcessed++;
                    Program._logWriter.WriteLine(tracksProcessed + " out of " + trackLinks.Count + " tracks processed\n");
                }
            }

            return trackList;
        }

        public IWebElement FindTrackElement(TrackData track)
        {
            try
            {
                var noResults = _driverWait.Until(ExpectedConditions.ElementExists(By.Id("trackSearchWarningMessage")));
                if (noResults.Text.Contains("No results."))
                    return null;

                _driverWait.Until(ExpectedConditions.ElementIsVisible(By.Id("tracksResult")));
            }
            catch (Exception ex)
            {
                Program._logWriter.WriteLine("Exception: " + ex);
                return null;
            }

            var primaryMetadataElements = _driver.FindElements(By.CssSelector("div.primaryMetadata.clickable"));
            var tracksLevenshteinRating = new Dictionary<IWebElement, TrackLevenshteinRating>();

            foreach (var element in primaryMetadataElements)
            {
                var metadataActionsElement = element.FindElement(By.XPath("./parent::*"));
                var metadataRowElement = metadataActionsElement.FindElement(By.XPath("./parent::*"));

                var title = metadataRowElement.FindElement(By.CssSelector("div.metadataActions div.primaryMetadata.clickable[title]")).Text;
                var artist = metadataRowElement.FindElement(By.CssSelector("div.secondaryMetadata a span[data-bind='text: primaryArtist.Name']")).Text;
                var album = metadataRowElement.FindElement(By.CssSelector("div.secondaryMetadata a span[data-bind='text: parentAlbum.Name']")).Text;

                var trackRating = new TrackLevenshteinRating
                {
                    Title = ComputeLevenshteinDistance(title, track.Title),
                    Artist = ComputeLevenshteinDistance(artist, track.Artist),
                    Album = ComputeLevenshteinDistance(album, track.Album)
                };
                tracksLevenshteinRating.Add(metadataActionsElement, trackRating);
            }

            IWebElement trackElement;

            // Find element with rating sum 0 (this means exact match)
            var trackMatches = tracksLevenshteinRating.Select(elem => elem).Where(elemValue => (elemValue.Value.Title + elemValue.Value.Artist + elemValue.Value.Album) == 0);
            var keyValuePairs = trackMatches as KeyValuePair<IWebElement, TrackLevenshteinRating>[] ?? trackMatches.ToArray();
            if (keyValuePairs.Any())
            {
                trackElement = keyValuePairs.First().Key;
            }
            else
            {
                // If not found, find element with lowest rating sum of title, artist and album
                trackElement = tracksLevenshteinRating.Aggregate((elem1, elem2) =>
                {
                    var sumElem1 = elem1.Value.Title + elem1.Value.Artist + elem1.Value.Album;
                    var sumElem2 = elem2.Value.Title + elem2.Value.Artist + elem2.Value.Album;
                    return sumElem1 < sumElem2 ? elem1 : elem2;
                }).Key;
            }

            return trackElement;
        }

        public bool AddTrackToPlaylist(IWebElement trackElement, string playlist)
        {
            var trackTitle = "";
            try
            {
                trackTitle = trackElement.FindElement(By.CssSelector("div.primaryMetadata.clickable[title]")).Text;
                var selector = trackTitle.Contains("\"") ? "div.primaryMetadata.clickable[title='" + trackTitle + "']" : "div.primaryMetadata.clickable[alt=\"" + trackTitle + "\"]";
                _driverWait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(selector)));

                _driverWait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("a[title='" + playlist + "']")));

                var action = new Actions(_driver);
                action.MoveToElement(trackElement).Perform();

                var addElement = _driverWait.Until((driver) =>
                {
                    var buttonAddElement = trackElement.FindElement(By.CssSelector("div.buttonGroup.actions > button.iconRowAdd"));
                    if (buttonAddElement.Displayed && trackElement.Displayed)
                        return buttonAddElement;
                    return null;
                });

                if (addElement == null)
                {
                    Program._logWriter.WriteLine("Could not add track: " + trackTitle + "\nError: Element not found");
                }

                addElement.Click();

                _driverWait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("button[title='Add to " + playlist + "']"))).Click();
                //_driverWait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("button[title='Legg til i " + playlist + "']"))).Click();

                return true;
            }
            catch (System.InvalidOperationException ex)
            {
                Program._logWriter.WriteLine("Could not add track: " + trackTitle + "\n\n" + ex);
                return false;
            }
        }

        private string ConvertAsciiValuesToCharacters(string source)
        {
            var regex = new Regex("&#(.*);");
            var match = regex.Match(source);

            if (match.Groups.Count < 2)
                return source;

            var code = Convert.ToInt32(match.Groups[1].ToString());
            var character = (char)code;

            return Regex.Replace(source, "&.*?;", character.ToString());
        }

        /*
        http://www.dotnetperls.com/levenshtein
        */
        private int ComputeLevenshteinDistance(string s, string t)
        {
            var n = s.Length;
            var m = t.Length;
            var d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (var i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (var j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (var i = 1; i <= n; i++)
            {
                //Step 4
                for (var j = 1; j <= m; j++)
                {
                    // Step 5
                    var cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }
    }
}
