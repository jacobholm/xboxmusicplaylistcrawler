﻿using System;

namespace XboxMusicPlaylistCrawler
{
    class LogWriter
    {
        private static System.IO.StreamWriter _streamWriter;

        public LogWriter(string file)
        {
            _streamWriter = new System.IO.StreamWriter(file);
        }

        public void WriteLine(string output)
        {
            _streamWriter.WriteLine(output);
            Console.WriteLine(output);
        }

        public void Close()
        {
            _streamWriter.Close();
        }
    }
}
