﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;


namespace XboxMusicPlaylistCrawler
{
    public struct TrackData
    {
        public string Title;
        public string Artist;
        public string Album;
    }

    class Program
    {
        public static LogWriter _logWriter;

        private static DriverManager _driverManager;

        static void Main(string[] args)
        {
            try
            {
                _logWriter = new LogWriter("log.txt");
                if (args.Count() != 1)
                {
                    _logWriter.WriteLine("Please give me the name of the playlist you want to add the tracks to in Xbox Music");
                    return;
                }
                var playlistName = args[0];

                _driverManager = new DriverManager();

                _logWriter.WriteLine("Loading tracks - " + DateTime.Now + "\n");
                var trackLinks = ReadTrackLinksFromFile("tracklinks.txt");
                var playlistTracks = _driverManager.GetPlaylistTracks(trackLinks);
                _logWriter.WriteLine("\nSuccessfully loaded " + playlistTracks.Count + " out of " + trackLinks.Count + " tracks - " + DateTime.Now);

                _logWriter.WriteLine("\n\n==================== Processing " + playlistTracks.Count + " track(s) ====================\n\n\n");

                _driverManager.Start();
                _driverManager.NavigateToUrl("http://music.xbox.com/");
                _driverManager.WaitUntil(ExpectedConditions.ElementIsVisible(By.CssSelector("a[title=\"" + playlistName + "\"]")));

                var tracksAdded = 0;
                foreach (var track in playlistTracks)
                {
                    _driverManager.SearchForTrack(track);

                    var trackElement = _driverManager.FindTrackElement(track);
                    if (trackElement == null)
                    {
                        _logWriter.WriteLine("\n-------------------- Track --------------------");
                        _logWriter.WriteLine("Title: " + track.Title + "\nArtist: " + track.Artist + "\nAlbum: " + track.Album);
                        _logWriter.WriteLine("Track was not found\n");
                        continue;
                    }

                    if (_driverManager.AddTrackToPlaylist(trackElement, playlistName))
                        tracksAdded++;
                    else
                    {
                        _logWriter.WriteLine("\n-------------------- Track --------------------");
                        _logWriter.WriteLine("Title: " + track.Title + "\nArtist: " + track.Artist + "\nAlbum: " + track.Album);
                        _logWriter.WriteLine("Track was found, but could not be added\n");
                    }
                }

                _logWriter.WriteLine("Found and added " + tracksAdded + " out of " + playlistTracks.Count + "tracks");

                // NB!! Change this to wait for an element to appear
                // We do this because the last addTrackToPlaylist() call doesn't get enough time to finish before the program terminates
                System.Threading.Thread.Sleep(1500);
            }
            catch (Exception ex)
            {
                _logWriter.WriteLine("\n\n\n\nERROR, OH NOES:\n\n" + ex);
            }
            finally
            {
                _logWriter.WriteLine("\nDone processing, see log.txt for more information - " + DateTime.Now);
                _logWriter.Close();

                if(_driverManager != null)
                    _driverManager.Quit();
            }
        }

        static List<string> ReadTrackLinksFromFile(string file)
        {
            try
            {
                var trackLinks = new List<string>();
                var streamReader = new System.IO.StreamReader(file);

                string trackLink;
                while ((trackLink = streamReader.ReadLine()) != null)
                {
                    if (trackLink.Contains("local"))
                        _logWriter.WriteLine("Skipping local track:\n" + trackLink + "\n");
                    else
                        trackLinks.Add(trackLink);
                }

                return trackLinks;
            }
            catch (System.IO.FileNotFoundException)
            {
                _logWriter.WriteLine("Could not find \"trackLinks.txt\", please create this file and paste your Spotify track http links into it");
                throw;
            }
        }
    }
}
